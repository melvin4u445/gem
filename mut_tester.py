import os
import shutil  #To copy files
import time
import subprocess
from subprocess import Popen, PIPE  #To run the testcases
import py_compile  #To compile programs manually
from decimal import Decimal #To calculate mutation score
test_name=file_name= "" 
killed = alive = error = unprocessed = 0
file_number = input_type = 0
mutant_folder= bat1= bat2= bat3=""

def tester(file_list_x):
    global input_type, test_name, isTestingOrg, new_file, alive, killed, error, unprocessed, file_number, file_name, mutant_folder, bat1, bat2
    
    fe = open(mutant_folder+'/coverage_log.txt', 'a')
    if input_type == 1:
        print file_list_x
        try:
            py_compile.compile(file_name, doraise=True)
            print "Compiled"
        except:
            print "Error while compiling"
        
        python_scripts = [test_name]
        #args = ' -v'     
        procs = []
        for f in python_scripts:
            procs.append(Popen(f, shell=True,stdout=PIPE,stderr=PIPE))
        results = []        
        while procs:
            results.append (procs.pop(0).communicate())
    
        if file_number==0: #Check if the original program runs against the test case without errors
            test1= str(results)
            if test1.find("OK") == -1:
                print "Source program failed against test cases"
                #exit("Please fix and try again")
            else: 
                print "Source program ran against the test cases without any failures"
                file_number += 1
            print "Test result1", test1
            return test1
        
        test2= str(results)
        
        print "Main", main.test1
        print "Test", test2
        
        if test2.find("OK") != -1:    
            print "Mutant Alive (No!)\n"
            shutil.move(file_list_x, mutant_folder+"/processed/alive")
            fe.write("   ALIVE")
            alive+=1
    
        elif test2.find("FAIL") != -1:
            print "Mutant Killed (Yes!)\n"
            shutil.move(file_list_x, mutant_folder+"/processed/killed")
            fe.write("   KILLED")
            killed+=1
        
        elif test2.find("Traceback") != -1:
            print "Error in file. File moved to error folder"
            shutil.move(file_list_x, mutant_folder+"/processed/error")
            fe.write("   ERROR")
            error+=1
            
        else:
            print "Unprocessed"
            shutil.move(file_list_x, mutant_folder+"/processed/unprocessed")
            fe.write("   UNPROCESSED")
            unprocessed+=1

        return results
    
    elif input_type == 2:
        os.system(bat1) #Autorun all the five django projects and house java project
        time.sleep(4)
        os.system(bat2)  
             
        print "\n================================================================"
        print file_list_x
        print "\nRunning Tron"
        cmd = bat3
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        (out, err) = process.communicate()
        print "*****TRON OUTPUT*****\n", out[-200:],"*********************"
        
        os.system('kill.bat') #Autorun all the five django projects and house java project
        isSubstring = "PASSED" in out #"Passed" or "Failed"    
        
        if isSubstring == True and file_number!=0:
            print "Django Mutant alive (No!)"
            shutil.move(file_list_x, mutant_folder+"/processed/alive/")
            fe.write("   ALIVE")
            alive+=1
            
        elif isSubstring == False and file_number!=0:
            print "Django Mutant killed (Yes!)"
            shutil.move(file_list_x, mutant_folder+"/processed/killed/")
            fe.write("   KILLED")
            killed+=1
        else:
            if file_number!= 0:
                print "Unprocessed"
                shutil.move(file_list_x, mutant_folder+"/processed/unprocessed")
                fe.write("   UNPROCESSED")
                unprocessed+=1

        if isSubstring == True and file_number==0:
            print "Orginal project Tested successfully against test cases \n\n"
            file_number += 1
            
        elif isSubstring == False and file_number==0:
            print "Orginal project Failed against test cases"
            #exit("Please fix and try again")

    fe.close()
    
def coverage():    
    global mutant_folder
    fe = open(mutant_folder+'/coverage_log.txt', 'r')
    fl = open(mutant_folder+'/log1.txt','w')
    
    for line in fe.readlines():
        line=line.strip()
        parts=line.split()
        if len(parts)>1:
            fl.write(parts[0] +"\t"+ parts[1]+"\n")
    fe.close()
    fl.close()
    shutil.move(mutant_folder+'/log1.txt',mutant_folder+'/coverage_log.txt')

#===============================================================================
# #Check mutation score
#===============================================================================

def mutation_score():
    global killed,alive, unprocessed, error
    print "\n******************************************"
    print "Unprocessed", unprocessed
    print "Error", error
    print "Killed Mutants", killed
    print "Alive Mutants", alive
    
    total=killed+alive   
    if total==0:  #To prevent zero by zero (0/0 error)
        print "Mutation score= 0%"
    else:
        score=Decimal(killed)/Decimal(total)*100
        score=round(score,2)
        print "Mutation score= ", score, "%"
    print "******************************************"
        
#===============================================================================
################################################################################
#===============================================================================

def main():
    global file_name, test_name, mutant_folder, bat1, bat2, bat3
    
    
    print "Testing original program"

    shutil.copy(file_name, file_name + '_org.py')
    main.test1= tester(file_name)
    try:
        file_list = os.listdir(mutant_folder+"/generated")
    except:
        exit("Please select the folder named 'generated', which contains the mutants you wish to test ")
    try:
        file_list = sorted(file_list, key=lambda x: int(x.split('_')[1]))  
        print file_list
    except:
        error="Cannot sort (Probably because files are renamed manually). Mutants will be tested without being sorted"
    
    total_files=len(file_list)
    print "Mutants detected in the folder are:", total_files
    
    for x in range(total_files):
        file_list[x]=mutant_folder+"/generated/"+file_list[x]
        shutil.copy(file_list[x], file_name)
        tester(file_list[x])
    
    shutil.copy(file_name + '_org.py', file_name)
