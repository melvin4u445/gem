1. Install Eclipse or PyCharm
2. Install Python
3. Install Django 1.3.0
4. Import the files in GeM folder to the workspace

To generate mutants
-------------------
5. Execute ui_mut_generator.py file to generate mutants
5.1 Input file: Select the file to be mutated 
5.2 Output folder: Select the output folder
5.3 Select the desired mutant operators and press the "Mutate" button

To kill mutants
---------------
6. Execute ui_mut_tester.py file to test and kill the generated mutants
6.1 Original Program: Select the same file that was selected in 5.1
6.2 Test file: Select the file containing test cases (Not needed for Model Based Systems)
6.3 Mutant Folder: Select the folder containing the generated mutants
6.4 Batch file 1, 2 and 3 are script files for automating model based testing. (Example of batch files can be found in "Batch" folder)
6.5 Kill Code Mutants: Select this if the mutants are generated from a normal program
6.6 Kill Model Mutants: Select this if the mutants should be tested against a model.
6.7 Press the "Test" button to start testing.




Email me at melvin.mathews@hotmail.com for suggestions or help.  

