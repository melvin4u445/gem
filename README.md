GeM - A mutation testing tool
=============================

The lack of generic mutation tools that can mutate simple texts and program codes led to the development of GeM. GeM is a mutation tool implemented in Python that is primarily targeted at mutating Python programs, but also allows one to generically define their own mutation operators in order to mutate different types of text-based artefacts, like code in different programming languages or even textual log files.

This tool stands out from other mutation tools for different reasons:

- It can mutate a Python program and test all the generated mutants against a test suite or it can mutate the source code of a program and run it against a test model. Mutational tools that are currently available in the market are either designed to mutate a code and test it against a test suite or to mutate a code and test it against a test model. Our tool can be used for testing mutants against a test suite or a test model, which makes it special compared to other tools.

- Another feature that makes GeM notable is its capability to generate mutants and store them in separate files. To facilitate this, GeM is sub-divided into two modules. One of them is the Mutant Generator, which generates the mutants and stores them as separate files and the other one is the Mutant Tester, which runs the generated mutants against the test suite or test model to study the effect it has on the system. Unlike other tools in the market, which generate mutants in each execution, GeM can generate a set of mutants with the Mutant Generator module and then test them repeatedly any number of times, with the Mutant Tester module. Hence, GeM saves the time needed to generate mutants each time.

- GeM can also be used to mutate plain text if necessary. For instance, consider a scenario where the behaviour of the IUT is impacted by a log file present in the system. GeM can generate mutants for this log file and it can automatically replace the original log file with the mutants to track the changes in the behaviour of the IUT. Mutants that were created manually or by some other tool can also be fed to GeM and be tested against a test suite or test model.

- GeM has two Graphical User Interfaces, one for the Mutant Generator and another one for the Mutant Tester. The user interface of the Mutant Generator module enables the user to apply as many as 17 mutation operators on a code or plain text and it has options to enable and disable the ability to check coverage. The user can also choose to mutate or not mutate comments. The user interface of the Mutant Tester has provisions to accept as input, the original code, the test suite and the folder containing the generated mutants. Its capability is further enhanced by providing options to accept script files as input. These script files are written by the user to automate the execution of model-based testing and they are used by GeM to automate the testing of mutants of a model-based system.


Instructions
------------

- Install Eclipse or PyCharm
- Install Python
- Install Django 1.3.0
- Import the files in GeM folder to the workspace

To generate mutants
-------------------
- Execute ui_mut_generator.py file to generate mutants
- UI fields are:

1. **Input file:** Select the file to be mutated

2. **Output folder:** Select the output folder

3. **Mutate:** Select the desired mutant operators and press the "Mutate" button

To kill mutants
---------------
- Execute ui_mut_tester.py file to test and kill the generated mutants
- UI fields are:

1. **Original Program:** Select the same file that was selected while generating mutants

2. **Test file:** Select the file containing test cases

3. **Mutant Folder:** Select the folder containing the generated mutants

4. **Batch file:** Batch file 1, 2 and 3 are script files for automating model based testing. (Example of batch files can be found in "Batch" folder)

5. **Kill Code Mutants:** Select this if the mutants are generated from a normal program

6. **Kill Model Mutants:** Select this if the mutants should be tested against a model.

7. **Test:** Press the "Test" button to start testing.




Email me at melvin.mathews@hotmail.com for suggestions or help.  
