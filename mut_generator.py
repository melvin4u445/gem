import shutil  #To copy files
import re
import ast #For OIL Method
import sys
from encodings import string_escape
file_number=1  #Variable to save mutants in different file names
file_name= file_name1= new_file= ""
mutant_folder=""
check_coverage = mutate_comments = False

#===============================================================================
# #Generate mutants
#===============================================================================

def mutant_replacer(s, op, typem):
    
    s2 = s[:]        
    #Counting number of occurrences of op
    # Loops: i is for original operator, j is for replacement operator, k is for specifying the location of original operator
    for i in range(0,len(op)):
        op_loc= [n for n in xrange(len(s)) if s.find(op[i], n) == n]        
                
        #Replacing op
        x=len(op[i]) #Length of original operator to be replaced
        for k in range(0,len(op_loc)):
            for j in range(0,len(op)):
                if j != i:
                    if check_not_comment(s, op_loc[k]) == True:
                        s= s[:(op_loc[k])] + op[j] + s[(op_loc[k]+x):] #Replacing original op with mutant op
                        
                        #Add text in the mutated code to point to the mutated portion
                        text= "\n#*********MUTATED LINE IS ABOVE THIS LINE*************** \n#Operation: " + typem +":  " + op[i] + " replaced with " + op[j] + "\n#************************************************" 
                        s = mark_mutant(s, typem, text, op_loc[k])
                        
                        #Saving the mutated file to another folder
                        write_file(s, typem)
                        s=s2
                
#################################################################################

def mutant_deleter(s, op, typem):
    
    s2 = s[:]
        
    #Counting number of occurrences of op
    # Loops: i is for original operator, j is for replacement operator, k is for specifying the location of original operator
    for i in range(0,len(op)):
        op_loc= [n for n in xrange(len(s)) if s.find(op[i], n) == n]
        
        #Replacing op
        x=len(op[i]) #Length of original operator to be replaced
        for k in range(0,len(op_loc)):
            if check_not_comment(s, op_loc[k]) == True:
                s= s[:(op_loc[k])] + "" + s[(op_loc[k]+x):] #Replacing original op with mutant op
                        
                #Add text in the mutated code to point to the mutated portion
                text= "\n#******************MUTATED LINE IS ABOVE THIS LINE****************** \n#Operation: " + typem +":  " + op[i] + " deleted " + "\n#************************************************"
                s = mark_mutant(s, typem, text, op_loc[k])
                
                #Saving the mutated file to another folder
                write_file(s, typem)             
                s=s2


################################################################################

def coi(s, op, typem):
    
    s2 = s[:]
        
    #Counting number of occurrences of op
    # Loops: i is for original operator, j is for replacement operator, k is for specifying the location of original operator
    for i in range(0,len(op)):
        op_loc= [n for n in xrange(len(s)) if s.find(op[i], n) == n]
               
        #Replacing op
        x=len(op[i]) #Length of original operator to be replaced
        for k in range(0,len(op_loc)):
            if check_not_comment(s, op_loc[k]) == True:
                if op[i]!='in':
                    s= s[:(op_loc[k])+x] + " not" + s[(op_loc[k]+x):] #Replacing original op with mutant op
                else:
                    s= s[:(op_loc[k])] + "not " + s[(op_loc[k]):] #Replacing original op with mutant op
                        
                #Add text in the mutated code to point to the mutated portion
                text= "\n#******************MUTATED LINE IS ABOVE THIS LINE****************** \n#Operation: " + typem +":  not operator inserted" + "\n#************************************************"
                s = mark_mutant(s, typem, text, op_loc[k])
                
                #Saving the mutated file to another folder
                write_file(s, typem)             
                s=s2

################################################################################

def crp(s, op, typem):
    
    s2 = s[:]
        
    #Counting number of occurrences of op
    # Loops: i is for original operator, j is for replacement operator, k is for specifying the location of original operator
    op_loc= [n for n in xrange(len(s)) if s.find(op[0], n) == n]
            
    #Replacing op
    x=len(op[0]) #Length of original operator to be replaced
    for k in range(0,len(op_loc)):
        if check_not_comment(s, op_loc[k]) == True:
            s= s[:(op_loc[k])] + op[1] + s[(op_loc[k]+x):] #Replacing original op with mutant op
                    
            #Add text in the mutated code to point to the mutated portion
            text= "\n#******************MUTATED LINE IS ABOVE THIS LINE****************** \n#Operation: " + typem +":  " + op[0] + " replaced with " + op[1] + "\n#************************************************"
            s = mark_mutant(s, typem, text, op_loc[k])
            
            #Saving the mutated file to another folder
            write_file(s, typem)         
            s=s2

################################################################################

def ddl(s, op, typem):
    
    s2 = s[:]
        
    #Counting number of occurrences of op
    # Loops: i is for original operator, j is for replacement operator, k is for specifying the location of original operator
    op_loc= [n for n in xrange(len(s)) if s.find(op[0], n) == n]
            
    #Replacing op    
    for k in range(0,len(op_loc)):
        m=1 
        try:   
            while (s[op_loc[k]+ m])!= "\n":  
                m= m+1
        except IndexError:   
                error= "EOF"
                
        if check_not_comment(s, op_loc[k]) == True:
            s= s[:(op_loc[k])] + "" + s[(op_loc[k]+m):] #Replacing original op with mutant op
                    
            #Add text in the mutated code to point to the mutated portion
            text= "\n#******************MUTATED LINE IS ABOVE THIS LINE****************** \n#Operation: " + typem +": Decorator deleted " + "\n#************************************************"
            s = mark_mutant(s, typem, text, op_loc[k])
            
            #Saving the mutated file to another folder
            write_file(s, typem)         
            s=s2


################################################################################

def ehd(s, op, typem):
    
    s2 = s[:]
        
    #Counting number of occurrences of op
    # Loops: i is for original operator, j is for replacement operator, k is for specifying the location of original operator
    op_loc= [n for n in xrange(len(s)) if s.find(op[0], n) == n]
            
    #Replacing op    
    for k in range(0,len(op_loc)):
        m=1 
        try:   
            while (s[op_loc[k]+ m])!= "\n":  
                m= m+1
        except IndexError:   
                error= "EOF"
            
        if check_not_comment(s, op_loc[k]) == True:    
            n=1
            tab=""
            while (s[op_loc[k]- n])!= "\n" and (op_loc[k]-n) > 0: 
                
                if (s[op_loc[k]-n]=="\t" or s[op_loc[k]-n]==" "):            
                    tab= tab+s[op_loc[k]-n]
                    
                elif s[op_loc[k]-n] != "\n":            
                    tab=""
                n= n+1
            s= s[:(op_loc[k]+ m)] + "\n"+ tab +"\traise" + s[(op_loc[k]+m):] #Replacing original op with mutant op
            z=len("\n"+tab+"\t")        
            #Add text in the mutated code to point to the mutated portion
            text= "\n#******************MUTATED LINE IS ABOVE THIS LINE****************** \n#Operation: " + typem +":  Raise statement added " + "\n#************************************************"
            s = mark_mutant(s, typem, text, op_loc[k]+m+z)
            
            #Saving the mutated file to another folder
            write_file(s, typem)         
            s=s2


################################################################################

def exs(s, op, typem):
    
    s2 = s[:]
        
    #Counting number of occurrences of op
    # Loops: i is for original operator, j is for replacement operator, k is for specifying the location of original operator    
    op_loc= [n for n in xrange(len(s)) if s.find(op[0], n) == n]
            
    #Replacing op    
    for k in range(0,len(op_loc)):
        m=1 
        try:   
            while (s[op_loc[k]+ m])!= "\n":  
                m= m+1
        except IndexError:   
                error= "EOF"
         
        if check_not_comment(s, op_loc[k]) == True:
            n=1
            tab=""
            while (s[op_loc[k]- n])!= "\n" and (op_loc[k]-n) > 0: 
                
                if (s[op_loc[k]-n]=="\t" or s[op_loc[k]-n]==" "):            
                    tab= tab+s[op_loc[k]-n]
                    
                elif s[op_loc[k]-n] != "\n":            
                    tab=""  
                n= n+1     
            s= s[:(op_loc[k]+ m)] + "\n"+tab+"\tpass" + s[(op_loc[k]+m):] #Replacing original op with mutant op
            z=len("\n"+tab+"\t")
                    
            #Add text in the mutated code to point to the mutated portion
            text= "\n#******************MUTATED LINE IS ABOVE THIS LINE****************** \n#Operation: " + typem +": Pass statement added " + "\n#************************************************"
            s = mark_mutant(s, typem, text, op_loc[k]+m+z)
            
            #Saving the mutated file to another folder
            write_file(s, typem)         
            s=s2


################################################################################

def scd(s, op, typem):
    
    s2 = s[:]
        
    #Counting number of occurrences of op
    # Loops: i is for original operator, j is for replacement operator, k is for specifying the location of original operator
    op_loc= [n for n in xrange(len(s)) if s.find(op[0], n) == n]
            
    #Replacing op    
    for k in range(0,len(op_loc)):
        m=1 
        try:   
            while (s[op_loc[k]+ m])!= "\n":  
                m= m+1
        except IndexError:   
                error= "EOF"
        if check_not_comment(s, op_loc[k]) == True:        
            s= s[:(op_loc[k])] + "" + s[(op_loc[k]+m):] #Replacing original op with mutant op
                    
            #Add text in the mutated code to point to the mutated portion
            text= "\n#******************MUTATED LINE IS ABOVE THIS LINE****************** \n#Operation: " + typem +": Super calling Deleted" + "\n#************************************************"
            s = mark_mutant(s, typem, text, op_loc[k])
            
            #Saving the mutated file to another folder
            write_file(s, typem)         
            s=s2


################################################################################

def oil(s, typem):
    
    s2 = s[:]
    
    loc= [m.start() for m in re.finditer(r"\n",s)]
    #print loc
    
    tree = ast.parse(s)

    for node in ast.walk(tree):
        
        if isinstance(node, (ast.For, ast.While)):
            #print 'node:', node, 'at line:', node.lineno
            #print 'body of loop ends at:', node.body[-1].lineno
            line= node.body[-1].lineno
            op_loc= loc[line-1]
            
        
            if check_not_comment(s, op_loc) == True:     
                n=1
                tab=""
                while (s[op_loc- n])!= "\n" and (op_loc-n) > 0: 
                    
                    if (s[op_loc-n]=="\t" or s[op_loc-n]==" "):            
                        tab= tab+s[op_loc-n]
                        
                    elif s[op_loc-n] != "\n":            
                        tab="" 
                    n= n+1    
                s= s[:op_loc] + "\n"+tab+"break" + s[op_loc:] #Replacing original op with mutant op
                z=len("\n"+tab)
                
                         
                #Add text in the mutated code to point to the mutated portion
                text= "\n#******************MUTATED LINE IS ABOVE THIS LINE****************** \n#Operation: " + typem +": break statement added " + "\n#************************************************"
                s = mark_mutant(s, typem, text, op_loc+z)
                 
                #Saving the mutated file to another folder
                write_file(s, typem)             
                s=s2


#===============================================================================
# #Add text in the mutated code to point to the mutated portion
#===============================================================================

def mark_mutant(s, typem, text, op_location):
    global file_name1, file_number, mutant_folder, check_coverage
    #print check_coverage
    m= n= 1 
    tab=""   
    need_indent=False
    try:
        while (s[op_location+ n])!= "\n" and (op_location+n) < len(s): #Find End of Line. For mutating func def 

            if(s[op_location+n])==":" :
                need_indent=True
            else:
                if any(substring in (s[op_location+n]) for substring in ["\n","\r","\t"," "] )==False: 
                    need_indent=False
            n=n+1
    except IndexError:   
        error= "EOF"
    
    while (s[op_location- m])!= "\n" and (op_location-m) > 0: 
        
        if (s[op_location-m]=="\t" or s[op_location-m]==" "):            
            tab= tab+s[op_location-m]
            
        elif s[op_location-m] != "\n":            
            tab=""
        m= m+1
    
    else:
        if need_indent==True:  #Check if the mutated part is a function definition
            tab=tab+'\t'    #To indent the code

        new_file1= file_name1 + '_' + str(file_number)+ '_' + typem + '.py'
        if check_coverage==True:
            tab1=tab+'print "Mutated line covered"\n'
            tab2=tab+"fe = open ('"+mutant_folder+"/coverage_log.txt', 'a')\n"
            tab3=tab+'fe.write("'+("\n").encode('string_escape')+new_file1+'")\n' 
            tab4=tab+'fe.close()\n#************************************************\n'
            s= s[:(op_location + n)]+ text + "\n" + tab1 + tab2 + tab3 + tab4 + s[(op_location + n):]
            
        else:
            s= s[:(op_location + n)]+ text + "\n" + s[(op_location + n):]
        
        return s
    
#===============================================================================
# #Save the mutated program to consecutive files    
#===============================================================================

def write_file(s1, typem):
    global file_name, file_name1,new_file, file_number
    
    if (file_number%20)==0:
        sys.stdout.write('.')
        if (file_number%1000)==0: sys.stdout.write('\n')
    
    new_file= mutant_folder+'/generated/'+ file_name1 + '_' + str(file_number)+ '_' + typem + '.py' #Assigns a new name for the mutant file    
    fileHandle = open(new_file, 'a')
    fileHandle.write(s1)
    fileHandle.close()
    
    file_number += 1
    
#===============================================================================
# #Check if the detected mutant is a comment line  
#===============================================================================
    
def check_not_comment(s, op_location):
    if mutate_comments== False: #Don't mutate comments
        for m in range(op_location,0,-1):
            if s[m] == "\n": #The mutant is not in a comment line
                return True           
            
            elif s[m] == "#": #The mutant is in a comment line
                return False
    else: #Then mutate comments
        return True
        
#===============================================================================
################################################################################
#===============================================================================

def main(s):
    global file_name, file_name1, mutant_folder

    fname= re.split("\/" , file_name)  #spliting file name
    flen=len(fname)
    file_name1= fname[flen-1] #The file name alone, ex. code.py
    shutil.copy(file_name, mutant_folder+'/'+'_org.py')
    shutil.copy(file_name, file_name + '_org.py')
