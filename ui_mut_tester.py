from Tkinter import *
import tkFileDialog, Tkinter
import mut_tester
import os
import tkMessageBox


top = Tk()

#Browse source file function
def browse_source_file():
    fl = tkFileDialog.askopenfile(parent=top,mode='rb',title='Choose the program that was mutated')    
    if fl != None:        
        print "The program that was mutated is:", fl.name      
        mut_tester.file_name=fl.name      
        #cb = Label(top, text =fl.name).grid(row=1, column=2, columnspan=5)
        fl.close()
                
#Browse test file function
def browse_test_file():
    fl = tkFileDialog.askopenfile(parent=top,mode='rb',title='Choose relevant test cases')
    if fl != None: 
        print "The selected test case is:", fl.name       
        mut_tester.test_name=fl.name        
        #cb = Label(top, text =fl.name).grid(row=2, column=2, columnspan=5)
        fl.close()

#Browse source file function
def browse_mutant_folder():
    conf=open("config.txt",'r')
    for line in conf.readlines():
        line=line.strip()
        parts=line.split(' :: ')
        for i in range(0, len(parts)):
            if parts[i]=='initialdir':
                init=parts[i+1]
    conf.close()
    
    fl = tkFileDialog.askdirectory(parent=top, title='Choose the folder containing the generated mutants', initialdir=init)    
    if fl != None: 

        #cb = Label(top, text =fl).grid(row=3, column=2, columnspan=5)
        fl=fl.rsplit('/',1)[0]
        mut_tester.mutant_folder=fl
        directory=[fl+'/processed', fl+'/processed/alive', fl+'/processed/error', fl+'/processed/killed', fl+'/processed/unprocessed' ]
        print "The following folder are being created for saving the output:"
        for x in range(0,len(directory)):
            print directory[x]
            if not os.path.exists(directory[x]):
                os.makedirs(directory[x])
                print "Folder created"
            else:
                print "Folder exists"
                

#Browse source file function
def browse_bat1():
    fl = tkFileDialog.askopenfile(parent=top,mode='rb',title='Choose bat file 1')    
    if fl != None:     
        print "The bat file to start the IUT(Implementation Under Test) is:", fl.name
        mut_tester.bat1=fl.name      
        #cb = Label(top, text =fl.name).grid(row=4, column=2, columnspan=5)
        fl.close()
        
#Browse source file function
def browse_bat2():
    fl = tkFileDialog.askopenfile(parent=top,mode='rb',title='Choose bat file 2')    
    if fl != None: 
        print "The bat file to start the Adapter is:", fl.name   
        mut_tester.bat2=fl.name      
        #cb = Label(top, text =fl.name).grid(row=5, column=2, columnspan=5)
        fl.close()

def browse_bat3():
    fl = tkFileDialog.askopenfile(parent=top,mode='rb',title='Choose bat file 2')    
    if fl != None:    
        print "The bat file to start TRON is:", fl.name 
        mut_tester.bat3=fl.read()    
        #cb = Label(top, text =fl.name).grid(row=6, column=2, columnspan=5)
        fl.close()

#Display a label
var = StringVar()
label = Label( top, textvariable=var )
var.set("Select the following:")
label.grid(row=0, column=0, columnspan=4)

#Functions performed when mutate button is pressed
def do_mutation():
    mut_tester.input_type = rad.get()
    print "Type of input is: " + str(mut_tester.input_type)  
    
    invalid_input= False
    text1=""
    if mut_tester.file_name == "":
        text1= text1+"The original code that was mutated\n"
        invalid_input= True
        
    if mut_tester.test_name=="" and mut_tester.input_type==1:
        text1= text1+ "A relevant test case file\n"
        invalid_input= True
        
    if mut_tester.mutant_folder == "":
        text1= text1+ "The folder containing mutants\n"
        invalid_input= True
    
    
    if mut_tester.bat1=="" and mut_tester.input_type==2:
        text1= text1+ "Batch file 1: The script to turn on IUT\n"
        invalid_input= True
        
    if mut_tester.bat2=="" and mut_tester.input_type==2:
        text1= text1+ "Batch file 2: The script to turn on the adapter\n"
        invalid_input= True
        
    if mut_tester.bat3=="" and mut_tester.input_type==2:
        text1= text1+ "Batch file 3: The script to turn on Tron\n"
        invalid_input= True
        
    if mut_tester.input_type == 0: 
        text1= text1+ "The type of program (Normal/Django)\n"
        invalid_input= True
    
    if invalid_input==True:
        tkMessageBox.showerror("Mandatory", "You forgot to choose:\n"+text1)
        
    else:        
        
        mut_tester.main()
        mut_tester.coverage()
        mut_tester.mutation_score()
        #exit()

##Browse source program file#################

file1=StringVar()
cb = Tkinter.Button(top, text ="Original Program", command = browse_source_file).grid(row=1, column=0, columnspan=2, sticky=W)
cb = Label(top, text="The original program that was mutated").grid(row=1, column=1, columnspan=2, sticky=W)
##Browse test file#################

file1=StringVar()
cb = Tkinter.Button(top, text ="        Test file         ", command = browse_test_file).grid(row=2, column=0, columnspan=2, sticky=W)
cb = Label(top, text="(Not applicable for Model based systems)").grid(row=2, column=1, columnspan=2, sticky=W)

file1=StringVar()
cb = Tkinter.Button(top, text ="   Mutant Folder   ", command = browse_mutant_folder).grid(row=3, column=0, columnspan=2, sticky=W)
cb = Label(top, text="The folder containing mutants").grid(row=3, column=1, columnspan=2, sticky=W)

cb = Label(top, text=" ").grid(row=4, column=1, columnspan=2, sticky=W)

file1=StringVar()
cb = Tkinter.Button(top, text ="Batch file 1", command = browse_bat1).grid(row=5, column=0, columnspan=2, sticky=W)
cb = Label(top, text="Batch script to start the IUT").grid(row=5, column=1, columnspan=2, sticky=W)

file1=StringVar()
cb = Tkinter.Button(top, text ="Batch file 2", command = browse_bat2).grid(row=6, column=0, columnspan=2, sticky=W)
cb = Label(top, text="Batch script to start the adapter").grid(row=6, column=1, columnspan=2, sticky=W)

file1=StringVar()
cb = Tkinter.Button(top, text ="Batch file 3", command = browse_bat3).grid(row=7, column=0, columnspan=2, sticky=W)
cb = Label(top, text="Batch script to start TRON").grid(row=7, column=1, columnspan=2, sticky=W)

## Option to select between mutating a django project and mutating a normal program
rad=IntVar()
rb= Radiobutton(top, text="Kill Code Mutants", variable=rad, value=1).grid(row=8, column=0, sticky=W)
rb= Radiobutton(top, text="Kill Model Mutants", variable=rad, value=2).grid(row=9, column=0, sticky=W)
####Mutate button######################
cb = Tkinter.Button(top, text ="Test", command = do_mutation).grid(row=10, column=1)

top.mainloop()
