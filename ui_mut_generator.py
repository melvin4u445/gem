from Tkinter import *
import tkFileDialog, Tkinter, shutil, os
import mut_generator
from time import strftime
import tkMessageBox
s= ""
top = Tk()

operator_selected="No"

#Browse source file function
def browse_source_file():
    fl = tkFileDialog.askopenfile(parent=top,mode='rb',title='Choose the program to be mutated')    
    if fl != None: 
        global s       
        s= fl.read()  
        mut_generator.file_name=fl.name
        print "The program to be mutated is:", fl.name        
        #cb = Label(top, text =fl.name).grid(row=8, column=1, columnspan=5)
        fl.close()
                
def browse_mutant_folder():
    conf=open("config.txt",'r')
    for line in conf.readlines():
        line=line.strip()
        parts=line.split(' :: ')
        print parts
        for i in range(0, len(parts)):
            if parts[i]=='initialdir':
                init=parts[i+1]
                print init            
    conf.close()       


    fl = tkFileDialog.askdirectory(parent=top, title='Choose the folder for saving mutants',initialdir=init)    
    if fl != None: 
        tm= strftime("%d.%b.%H.%M.%S")
        mut_generator.mutant_folder=fl+'/'+tm
        conf=open("config.txt",'w')
        conf.write('initialdir :: '+fl+'/'+tm)
        conf.close()   
        directory=fl+'/'+tm+'/generated'
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        print "The generated mutants will be saved in:", directory    
        #cb = Label(top, text =fl).grid(row=9, column=1, columnspan=5)

    
#Functions performed when mutate button is pressed
def do_mutation():
    #To select if the user wants to mutate a normal program or a django project
    global operator_selected, s

    invalid_input= False
    text1= ""
    if mut_generator.file_name == "":
        text1 = text1+"The code to be mutated(Input file)\n"
        invalid_input= True
        
    if mut_generator.mutant_folder == "":
        text1 = text1+"The folder for generating mutants(Output folder)\n"
        invalid_input= True
        
    if invalid_input== True:
        tkMessageBox.showerror("Mandatory", "You forgot to choose:\n"+text1)

    else:
        mut_generator.main(s) #Running the main function
        #print "Generating mutants\n"
        print "Please wait"
        
        if check_coverage.get()==1:
            mut_generator.check_coverage=True
            
        if mutate_comments.get()==1:
            mut_generator.mutate_comments=True
    
        
        if aod.get()==1:    
            op=['+','-','*','/','%','**','//']
            typem='AOD'
            operator_selected="Yes"
            mut_generator.mutant_deleter(s, op, typem)
        
        if aor.get()==1:
            op=['+','-','*','/','%','**','//']
            typem='AOR'
            operator_selected="Yes"
            mut_generator.mutant_replacer(s, op, typem)
            
        if asr.get()==1:
            op=['=','+=','-=','*=','/=','%=','**=','//=']
            typem='ASR'
            operator_selected="Yes"
            mut_generator.mutant_replacer(s, op, typem)
            
        if bcr.get()==1:
            op=['break','continue']
            typem='BCR'
            operator_selected="Yes"
            mut_generator.mutant_replacer(s, op, typem)
        
        if cod.get()==1:
            op=['not']
            typem='COD'
            operator_selected="Yes"
            mut_generator.mutant_deleter(s, op, typem)
            
        if coi.get()==1:
            op=['if', 'while', 'is', 'in']
            typem='COI'
            operator_selected="Yes"
            mut_generator.coi(s, op, typem)
    
        if crp.get()==1:
            cr1 = crp1.get()
            cr2 = crp2.get()
            op=[cr1, cr2]
            typem='CRP'
            operator_selected="Yes"
            mut_generator.crp(s, op, typem)
            
            
        if ddl.get()==1:
            op=['@']
            typem='DDL'
            operator_selected="Yes"
            mut_generator.ddl(s, op, typem)
    
        if ehd.get()==1:
            op=['except']
            typem='EHD'
            operator_selected="Yes"
            mut_generator.ehd(s, op, typem)
            
        if exs.get()==1:
            op=['except']
            typem='EXS'
            operator_selected="Yes"
            mut_generator.exs(s, op, typem)
            
        if lcr.get()==1:
            op=['and','or']
            typem='LCR'
            operator_selected="Yes"
            mut_generator.mutant_replacer(s, op, typem)
    
        if lod.get()==1:
            op=['~']
            typem='LOD'
            operator_selected="Yes"
            mut_generator.mutant_deleter(s, op, typem)
        
        if lor.get()==1:
            op=['&', '|', '^', '<<','>>']
            typem='LOR'
            operator_selected="Yes"
            mut_generator.mutant_replacer(s, op, typem)
    
            
        if ror.get()==1:
            op=['>','>=','<','<=','==','!=']
            typem='ROR'
            operator_selected="Yes"
            mut_generator.mutant_replacer(s, op, typem)    
                
        if scd.get()==1:
            op=['super']
            typem='SCD'
            operator_selected="Yes"
            mut_generator.scd(typem)
            
        if oil.get()==1:
            typem='OIL'
            operator_selected="Yes"
            mut_generator.oil(s, typem)
    
        if man.get()==1:
            mn1 = man1.get()
            mn2 = man2.get()
            op=[mn1, mn2]
            typem='Manual'
            operator_selected="Yes"
            mut_generator.mutant_replacer(s, op, typem)    
            
        if operator_selected=="No":
            tkMessageBox.showerror("Mandatory", "Select at least one mutation operator")
            
        else:
            print "\nMutant generation completed"
            print "Total number of mutants generated =", mut_generator.file_number-1
            
                
            shutil.copy(mut_generator.file_name + '_org.py', mut_generator.file_name)
            os.remove(mut_generator.file_name + '_org.py')
            exit()
        

#Display a label
var = StringVar()
label = Label( top, textvariable=var )
var.set("Select the mutant operators to apply:")
label.grid(row=0, column=0, columnspan=4)

#Check boxes
aod = IntVar()
aor = IntVar()
asr = IntVar()
bcr = IntVar()
cod = IntVar()
coi = IntVar()
crp = IntVar()
ddl = IntVar()
ehd = IntVar()
exs = IntVar()
lcr = IntVar()
lod = IntVar()
lor = IntVar()
ror = IntVar()
scd = IntVar()
oil = IntVar()
man = IntVar()


cb = Checkbutton(top, text = "AOD      ", variable = aod, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=1, column=0, sticky=W)
cb = Checkbutton(top, text = "AOR      ", variable = aor, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=2, column=0, sticky=W)
cb = Checkbutton(top, text = "ASR      ", variable = asr, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=3, column=0, sticky=W)
cb = Checkbutton(top, text = "BCR      ", variable = bcr, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=4, column=0, sticky=W)
cb = Checkbutton(top, text = "COD      ", variable = cod, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=5, column=0, sticky=W)

cb = Checkbutton(top, text = "COI      ", variable = coi, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=1, column=1, sticky=W)
cb = Checkbutton(top, text = "DDL      ", variable = ddl, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=2, column=1, sticky=W)
cb = Checkbutton(top, text = "EHD      ", variable = ehd, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=3, column=1, sticky=W)
cb = Checkbutton(top, text = "EXS      ", variable = exs, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=4, column=1, sticky=W)
cb = Checkbutton(top, text = "LCR      ", variable = lcr, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=5, column=1, sticky=W)

cb = Checkbutton(top, text = "LOD      ", variable = lod, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=1, column=2, sticky=W)
cb = Checkbutton(top, text = "LOR      ", variable = lor, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=2, column=2, sticky=W)
cb = Checkbutton(top, text = "ROR      ", variable = ror, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=3, column=2, sticky=W)
cb = Checkbutton(top, text = "SCD      ", variable = scd, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=4, column=2, sticky=W)
cb = Checkbutton(top, text = "OIL      ", variable = oil, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=5, column=2, sticky=W)

##CRP#################
cb = Checkbutton(top, text = "CRP     ", variable = crp, onvalue = 1, offvalue = 0, height=1, width = 5).grid(row=6, column=0, sticky=W)
crp1=StringVar()
crp2=StringVar()
e  = Entry(top,textvariable=crp1, width=10).grid(row=6, column=1, sticky=W)
e  = Entry(top,textvariable=crp2, width=10).grid(row=6, column=2, sticky=W)

##Manual Value########

cb = Checkbutton(top, text = "Manual", variable = man, onvalue = 1, offvalue = 0, height=1, width = 6).grid(row=7, column=0, sticky=W)
man1=StringVar()
man2=StringVar()
e  = Entry(top,textvariable=man1, width=10).grid(row=7, column=1, sticky=W)
e  = Entry(top,textvariable=man2, width=10).grid(row=7, column=2, sticky=W)

##Browse source program file#################

file1=StringVar()
cb = Tkinter.Button(top, text ="    Input file    ", command = browse_source_file).grid(row=8, column=1, columnspan=2, sticky=W)


file1=StringVar()
cb = Tkinter.Button(top, text ="Output folder", command = browse_mutant_folder).grid(row=9, column=1, columnspan=2, sticky=W)

check_coverage = IntVar()
cb = Checkbutton(top, text = "Check Coverage", variable = check_coverage, onvalue = 1, offvalue = 0, height=1).grid(row=10, column=0, columnspan=3, sticky=W)

mutate_comments = IntVar()
cb = Checkbutton(top, text = "Mutate comments", variable = mutate_comments, onvalue = 1, offvalue = 0, height=1).grid(row=11, column=0, columnspan=3, sticky=W)

####Mutate button######################
cb = Tkinter.Button(top, text ="Mutate", command = do_mutation).grid(row=16, column=1, columnspan=2, sticky=W)

top.mainloop()
